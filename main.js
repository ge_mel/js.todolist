function ToDoList (){
	this.todos = [];
}
ToDoList.prototype = {
	do: function(fnName){

		let name = '_'+fnName,
			args = Array.prototype.slice.call(arguments, 1);

		if(this[name]){
			this[name].apply(this, args);
		}

	},
	_add: function(todo){
		
		this.todos.push(todo);

		console.log(todo.title + ' added');
		console.log(this.todos);

	},
	_remove: function(todoid){

		console.log('to remove id: '+parseInt(this.todos.indexOf(this.todos.filter(item => item.id === parseInt(todoid))[0])));
		// this.todos.slice( parseInt(this.todos.indexOf(this.todos.filter(item => item.id === parseInt(todoid))[0])) , this.todos.indexOf(this.todos.filter(item => item.id === todoid)[0])+1 );
		if (this.todos.filter(item => item.id === parseInt(todoid)).length){

			this.todos = this.todos.filter(item => item.id !== parseInt(todoid));

			console.log('id: '+todoid + ' removed');
			console.log(this.todos);

		} else {
			
			console.log('todo id:'+todoid+' does not exist');
			
		}

	}
}



let mytodolis = new ToDoList, c = 0;
mytodolis.do('add', {title: 'my first to do', id: parseInt(c++, 10) , creationDate: new Date(), status: false });
mytodolis.do('add', {title: 'my second to do', id: parseInt(c++, 10) , creationDate: new Date(), status: false });
mytodolis.do('add', {title: 'my trird to do', id: parseInt(c++, 10) , creationDate: new Date(), status: false });
mytodolis.do('remove', 2);
mytodolis.do('remove', 78);
mytodolis.do('add', {title: 'another one todo', id: parseInt(c++, 10), creationDate: new Date(), status: false});